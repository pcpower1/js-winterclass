/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)

let thirteenInchPizza = 13
let seventeenInchPizza = 17


let areaOfThirteenInchPizza = Math.PI * (thirteenInchPizza / 2) ** 2 //Returns 132.73228961416876
let areaOfSeventeenInchPizza = Math.PI * (seventeenInchPizza / 2) ** 2 //Returns 226.98006922186255


// 2. What is the cost per square inch of each pizza?

let costOfThirteen = 16.99 / areaOfThirteenInchPizza //Returns 0.12800201103580125
let costOfSeventeen = 19.99 / areaOfSeventeenInchPizza //Returns 0.08806940657181973

console.log(costOfThirteen)
console.log(costOfSeventeen)


// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)


// 4. Draw 3 cards and use Math to determine the highest
// card

let cardOne = Math.round(Math.random() * 13 + 1) // Numbers 4 generated
let cardTwo = Math.round(Math.random() * 13 + 1) // Numbers 7  generated
let cardThree = Math.round(Math.random() * 13 + 1) // Numbers 2 generated

let highestCard = Math.max(cardOne, cardTwo, cardThree) // highestCard 7

console.log(highestCard)


/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

let firstName = "Morgan"
let lastName = "Corbridge"
let streetName = "123 My Street N"
let city = "Seattle"
let state = "WA"
let zipCode = "98321"

let mailingAddress = `
${firstName} ${lastName}
${streetName}
${city} ${state} ${zipCode}`

console.log(mailingAddress)





// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring


/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00

//let startDate = 1 / 1 / 2020
//let endDate = 04 / 01 / 2020


//var middate = new Date((startdate.getTime() + enddate.getTime()) / 2);

date1 = new Date("Jan 1 2020") //returns Wed Jan 01 2020 00:00:00 GMT-0800 (Pacific Standard Time)
date2 = new Date("apr 1 2020") //returns Wed Apr 01 2020 00:00:00 GMT-0700 (Pacific Daylight Time)
middle = new Date(date2 - (date2 - date1) / 2); //returns Sat Feb 15 2020 11:30:00 GMT-0800 (Pacific Standard Time)
console.log(middle); //returns sat Feb 15 2020 11:30 PST

//
// Look online for documentation on Date objects.

// Starting hint:
const endDate = new Date(2019, 3, 1);
